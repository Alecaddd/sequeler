⚠️⚠️ **SEQUELER IS CURRENTLY IN EARLY DEVELOPMENT, NOT READY TO BE USED FOR PRODUCTION!** ⚠️⚠️

# Sequeler
> Friendly SQL Client

Sequeler is a simple SQL GUI written in Rust and Gtk4

## Database support
**Database Type** | **Version** | **Support**
---|---|---
SQLite | - | -
MySQL | - | -
MariaDB | - | -
PostgreSQL | - | -
