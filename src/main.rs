/*
    This file is part of Sequeler.

    Sequeler is free software: you can redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    Sequeler is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
    even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License along with Sequeler. If not,
    see <https://www.gnu.org/licenses/>.

    Authored by: Alessandro "Alecaddd" Castellani <castellani.ale@gmail.com>
*/

mod widgets;

use widgets::CustomButton;
use std::rc::Rc;
use std::cell::Cell;

use gtk::prelude::*;
use gtk::{self, glib};

const APP_ID: &str = "com.github.alecaddd.sequeler";

fn main() -> glib::ExitCode {
    let app = gtk::Application::builder().application_id(APP_ID).build();
    app.connect_activate(build_ui);
    app.run()
}

fn build_ui(app: &gtk::Application) {
    let button_increase = CustomButton::with_label("Increase");
        button_increase.set_margin_top(12);
        button_increase.set_margin_bottom(12);
        button_increase.set_margin_start(12);
        button_increase.set_margin_end(12);

    let button_decrease = CustomButton::with_label("Decrease");
        button_decrease.set_margin_top(12);
        button_decrease.set_margin_bottom(12);
        button_decrease.set_margin_start(12);
        button_decrease.set_margin_end(12);

    // let number = Rc::new(Cell::new(0));
    let label = gtk::Label::builder()
        .label(&button_increase.number().to_string())
        .build();

    button_increase.connect_number_notify(glib::clone!(@weak label =>
        move |button| {
            label.set_label(&button.number().to_string());
        }
    ));

    button_decrease.connect_number_notify(glib::clone!(@weak label =>
        move |button| {
            label.set_label(&button.number().to_string());
        }
    ));

    let switch = gtk::Switch::new();
    switch.set_halign(gtk::Align::Center);

    let gtk_box = gtk::Box::builder()
        .orientation(gtk::Orientation::Vertical)
        .hexpand(false)
        .halign(gtk::Align::Center)
        .build();
    gtk_box.append(&button_increase);
    gtk_box.append(&button_decrease);
    gtk_box.append(&label);
    gtk_box.append(&switch);

    let window = gtk::ApplicationWindow::builder()
        .application(app)
        .title("Sequeler")
        .width_request(400)
        .height_request(400)
        .child(&gtk_box)
        .build();

    window.present();
}
